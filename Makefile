plant:
	mvn com.github.jeluard:plantuml-maven-plugin:generate

docker_run_dev:
	mvn clean install
	docker build --tag store-backend -f Dockerfile.dev .
	docker run -p 8080:8080 -e JAVA_OPTS=-Dspring.profiles.active=jpa,dev,h2 store-backend
