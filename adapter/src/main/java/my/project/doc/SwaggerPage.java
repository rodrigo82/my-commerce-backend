package my.project.doc;

public interface SwaggerPage {
    int getSize();

    int getPage();

    String getSort();
}
