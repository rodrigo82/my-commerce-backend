package my.project.adapter;

import lombok.*;
import my.project.port.Criteria;
import org.springframework.data.domain.Pageable;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@With
public class PagedCriteria<T> implements Criteria<T> {
    private Pageable pageable;
    private Criteria<T> criteria;
}
