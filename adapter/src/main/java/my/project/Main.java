package my.project;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import static org.springframework.boot.SpringApplication.run;

@Import({PersistenceConfig.class, ApiConfig.class})
@SpringBootApplication
public class Main {

    public static void main(String[] args) {
        run(Main.class);
    }

}
