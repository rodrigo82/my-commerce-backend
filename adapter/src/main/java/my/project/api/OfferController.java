package my.project.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import my.project.domain.model.Offer;
import my.project.port.Count;
import my.project.port.Search;
import my.project.domain.criteria.OfferCriteria;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("offers")
public class OfferController implements
        FindByIdController<Offer>,
        SearchController<Offer, OfferCriteria> {

    @Getter
    private Search<Offer> search;
    @Getter
    private Count<Offer> count;

}
