package my.project.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import my.project.domain.model.Product;
import my.project.port.Count;
import my.project.port.Search;
import my.project.domain.criteria.ProductCriteria;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("products")
public class ProductController implements
        FindByIdController<Product>,
        SearchController<Product, ProductCriteria> {

    @Getter
    private Search<Product> search;
    @Getter
    private Count<Product> count;

}
