package my.project.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

public interface FindByIdController<T> {

    @GetMapping("/{id}")
    default T findById(@PathVariable T id) {
        return id;
    }

}
