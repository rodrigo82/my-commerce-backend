package my.project.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import my.project.domain.model.Section;
import my.project.port.Count;
import my.project.port.Search;
import my.project.domain.criteria.SectionCriteria;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("sections")
public class SectionController implements
        FindByIdController<Section>,
        SearchController<Section, SectionCriteria> {

    @Getter
    private Search<Section> search;
    @Getter
    private Count<Section> count;

}
