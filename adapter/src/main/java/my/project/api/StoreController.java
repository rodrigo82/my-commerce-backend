package my.project.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import my.project.domain.model.Store;
import my.project.port.Count;
import my.project.port.Search;
import my.project.domain.criteria.StoreCriteria;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@AllArgsConstructor
@RestController
@RequestMapping("stores")
public class StoreController implements
        FindByIdController<Store>,
        SearchController<Store, StoreCriteria> {

    @Getter
    private Search<Store> search;
    @Getter
    private Count<Store> count;

}
