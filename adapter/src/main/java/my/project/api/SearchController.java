package my.project.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import my.project.adapter.PagedCriteria;
import my.project.port.Count;
import my.project.port.Criteria;
import my.project.port.Search;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;

public interface SearchController<T, C extends Criteria<T>> {

    Search<T> getSearch();

    Count<T> getCount();

    @GetMapping
    default Page<T> search(C criteria, Pageable page) {
        return (Page<T>) getSearch().search(new PagedCriteria<>(page, criteria));
    }

    @GetMapping("count")
    default CountResult count(C criteria) {
        return new CountResult(getCount().count(criteria));
    }

    @Data
    @AllArgsConstructor
    public static class CountResult {
        private Long count;
    }

}
