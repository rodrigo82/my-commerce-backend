package my.project;

import my.project.persistence.data.MockData;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EntityScan(basePackageClasses = PersistenceConfig.class)
@EnableJpaRepositories
@EnableTransactionManagement
@EnableSpringDataWebSupport
@Import({MockData.class})
public class PersistenceConfig {
}
