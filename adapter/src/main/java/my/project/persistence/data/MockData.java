package my.project.persistence.data;

import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import my.project.domain.model.*;
import org.springframework.data.repository.CrudRepository;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;

@org.springframework.context.annotation.Profile("h2")
@AllArgsConstructor
@Slf4j
public class MockData {

    private static final Date INITIAL_DATE = Date.from(
            LocalDateTime.of(2020, 1, 1, 0, 0).toInstant(ZoneOffset.UTC)
    );
    private static final Integer ORDER_COUNT = 1000;
    private static final Integer SECTION_COUNT = 10;
    private static final Integer CUSTOMER_COUNT = 1000;
    private static final Integer PRODUCT_COUNT = 400;
    private static final Integer[] OFFER_COUNT = {1, 10};
    private static final Integer[] PRICE = {1, 1000};
    private static final Integer PRICE_VARIATION = 30;
    private static final Integer STORE_COUNT = 30;

    private static final Faker FAKER = new Faker();
    private static final int ITEM_COUNT = 10;
    private static final int MAX_QUANTITY = 10;

    private final CrudRepository<Customer, String> customerRepository;
    private final CrudRepository<Product, String> productRepository;
    private final CrudRepository<Section, String> sectionRepository;
    private final CrudRepository<Store, String> storeRepository;
    private final CrudRepository<Offer, String> offerRepository;
    private final CrudRepository<SaleOrder, String> orderRepository;

    @PostConstruct
    public void init() {
        var sections = (List<Section>) sectionRepository.saveAll(createSections());
        var products = (List<Product>) productRepository.saveAll(createProducts(sections));
        var stores = (List<Store>) storeRepository.saveAll(createStores());
        var customers = (List<Customer>) customerRepository.saveAll(createCustomers());
        var offers = (List<Offer>) offerRepository.saveAll(createOffers(products, stores));
        var orders = (List<SaleOrder>) orderRepository.saveAll(createOrders(customers, offers));
        log.info("mock data created");
    }

    private List<SaleOrder> createOrders(List<Customer> customers, List<Offer> offers) {
        return range(0, ORDER_COUNT)
                .mapToObj(value -> createOrder(customers, offers))
                .collect(toList());
    }

    private SaleOrder createOrder(List<Customer> customers, List<Offer> offers) {
        var date = LocalDateTime.ofInstant(FAKER.date().between(INITIAL_DATE, new Date()).toInstant(), ZoneId.systemDefault());
        var payment = new Payment()
                .withDate(date.plus(FAKER.number().numberBetween(1, 10), MINUTES))
                .withStatus(FAKER.options().option(Payment.Status.class));
        var items = getItems(offers);
        return new SaleOrder()
                .withDate(date)
                .withCustomer(FAKER.options().nextElement(customers))
                .withStatus(getOrderStatus(date, payment))
                .withItems(items)
                .withPayment(payment);
    }

    private Set<Item> getItems(List<Offer> offers) {
        return range(0, FAKER.number().numberBetween(1, ITEM_COUNT))
                .mapToObj(value -> getItem(offers))
                .filter(distinctByKey(Item::getOffer))
                .collect(toSet());
    }

    private Item getItem(List<Offer> offers) {
        Offer offer = FAKER.options().nextElement(offers);
        return new Item()
                .withOffer(offer)
                .withPrice(offer.getPrice())
                .withQuantity(FAKER.number().numberBetween(1, MAX_QUANTITY));
    }

    private SaleOrder.Status getOrderStatus(LocalDateTime date, Payment payment) {
        if (date.isBefore(LocalDateTime.now().minus(7, DAYS)))
            return payment.getStatus().equals(Payment.Status.CONFIRMED)
                    ? SaleOrder.Status.FINISHED
                    : SaleOrder.Status.WAITING_PAYMENT;
        else
            return payment.getStatus().equals(Payment.Status.CONFIRMED)
                    ? FAKER.options().option(SaleOrder.Status.CONFIRMED, SaleOrder.Status.DELIVERING, SaleOrder.Status.FINISHED)
                    : SaleOrder.Status.WAITING_PAYMENT;
    }

    private List<Offer> createOffers(List<Product> products, List<Store> stores) {
        return products.stream()
                .map(product -> createOffers(product, stores))
                .flatMap(Collection::stream)
                .collect(toList());
    }

    private List<Offer> createOffers(Product product, List<Store> stores) {
        return range(0, FAKER.number().numberBetween(OFFER_COUNT[0], OFFER_COUNT[1]))
                .mapToObj(value -> {
                    var basePrice = FAKER.number().randomDouble(2, PRICE[0], PRICE[1]);
                    return createOffer(product, FAKER.options().nextElement(stores), basePrice);
                })
                .filter(distinctByKey(Offer::getStore))
                .collect(toList());
    }

    private Offer createOffer(Product product, Store store, double basePrice) {
        var priceVariation = (100.00 + FAKER.number().numberBetween(-PRICE_VARIATION, PRICE_VARIATION)) / 100.00;
        return new Offer()
                .withPrice(BigDecimal.valueOf(basePrice * priceVariation))
                .withProduct(product)
                .withStore(store);
    }

    private List<Store> createStores() {
        return range(0, STORE_COUNT)
                .mapToObj(value -> createStore())
                .filter(distinctByKey(Store::getTaxNumber))
                .filter(distinctByKey(Store::getName))
                .collect(toList());
    }

    private Store createStore() {
        return new Store()
                .withName(FAKER.company().name())
                .withDescription(FAKER.backToTheFuture().quote())
                .withTaxNumber(FAKER.code().imei());
    }

    private List<Section> createSections() {
        return range(0, SECTION_COUNT)
                .mapToObj(value -> createSection())
                .filter(distinctByKey(Section::getName))
                .collect(toList());
    }

    private Section createSection() {
        return new Section()
                .withName(FAKER.commerce().department());
    }

    private List<Product> createProducts(List<Section> sections) {
        return range(0, PRODUCT_COUNT)
                .mapToObj(value -> createProduct(sections))
                .filter(distinctByKey(Product::getName))
                .collect(toList());
    }

    private Product createProduct(List<Section> sections) {
        return new Product()
                .withDescription(FAKER.friends().quote())
                .withName(FAKER.commerce().productName())
                .withSection(FAKER.options().nextElement(sections));
    }

    private List<Customer> createCustomers() {
        return range(0, CUSTOMER_COUNT)
                .mapToObj(value -> getCustomer())
                .filter(distinctByKey(Customer::getEmail))
                .collect(toList());
    }

    private Customer getCustomer() {
        return new Customer()
                .withName(FAKER.name().fullName())
                .withEmail(FAKER.internet().emailAddress());
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

}

