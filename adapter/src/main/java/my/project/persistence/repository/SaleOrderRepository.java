package my.project.persistence.repository;

import my.project.domain.model.SaleOrder;
import my.project.persistence.adapter.FindAdapter;
import my.project.persistence.adapter.WriteAdapter;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface SaleOrderRepository extends CrudRepository<SaleOrder, String>, JpaSpecificationExecutor<SaleOrder>,
        FindAdapter<SaleOrder>,
        WriteAdapter<SaleOrder> {
}
