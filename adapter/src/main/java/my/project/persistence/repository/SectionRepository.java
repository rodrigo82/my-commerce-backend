package my.project.persistence.repository;

import my.project.domain.model.Section;
import my.project.persistence.adapter.translator.CriteriaTranslator;
import my.project.persistence.adapter.CountAdapter;
import my.project.persistence.adapter.FindAdapter;
import my.project.persistence.adapter.SearchAdapter;
import my.project.persistence.adapter.translator.SectionCriteriaTranslator;
import my.project.domain.criteria.SectionCriteria;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface SectionRepository extends CrudRepository<Section, String>, JpaSpecificationExecutor<Section>,
        FindAdapter<Section>,
        SearchAdapter<Section>,
        CountAdapter<Section> {

    @Override
    default CriteriaTranslator<Section, SectionCriteria> getCriteriaTranslator() {
        return new SectionCriteriaTranslator();
    }

}
