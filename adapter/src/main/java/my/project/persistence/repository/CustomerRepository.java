package my.project.persistence.repository;

import my.project.domain.model.Customer;
import my.project.persistence.adapter.FindAdapter;
import my.project.persistence.adapter.WriteAdapter;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, String>, JpaSpecificationExecutor<Customer>,
        FindAdapter<Customer>,
        WriteAdapter<Customer> {
}
