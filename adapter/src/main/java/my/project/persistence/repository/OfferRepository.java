package my.project.persistence.repository;

import my.project.domain.model.Offer;
import my.project.persistence.adapter.translator.CriteriaTranslator;
import my.project.persistence.adapter.translator.OfferCriteriaTranslator;
import my.project.persistence.adapter.CountAdapter;
import my.project.persistence.adapter.FindAdapter;
import my.project.persistence.adapter.SearchAdapter;
import my.project.domain.criteria.OfferCriteria;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface OfferRepository extends CrudRepository<Offer, String>, JpaSpecificationExecutor<Offer>,
        FindAdapter<Offer>,
        SearchAdapter<Offer>,
        CountAdapter<Offer>  {

    @Override
    default CriteriaTranslator<Offer, OfferCriteria> getCriteriaTranslator() {
        return new OfferCriteriaTranslator();
    }
}
