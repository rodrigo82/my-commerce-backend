package my.project.persistence.repository;

import my.project.domain.model.Store;
import my.project.persistence.adapter.translator.CriteriaTranslator;
import my.project.persistence.adapter.CountAdapter;
import my.project.persistence.adapter.FindAdapter;
import my.project.persistence.adapter.SearchAdapter;
import my.project.persistence.adapter.translator.StoreCriteriaTranslator;
import my.project.domain.criteria.StoreCriteria;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface StoreRepository extends CrudRepository<Store, String>, JpaSpecificationExecutor<Store>,
        FindAdapter<Store>,
        SearchAdapter<Store>,
        CountAdapter<Store> {

    @Override
    default CriteriaTranslator<Store, StoreCriteria> getCriteriaTranslator() {
        return new StoreCriteriaTranslator();
    }

}
