package my.project.persistence.repository;

import my.project.domain.model.Product;
import my.project.persistence.adapter.translator.CriteriaTranslator;
import my.project.persistence.adapter.translator.ProductCriteriaTranslator;
import my.project.persistence.adapter.CountAdapter;
import my.project.persistence.adapter.FindAdapter;
import my.project.persistence.adapter.SearchAdapter;
import my.project.domain.criteria.ProductCriteria;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, String>, JpaSpecificationExecutor<Product>,
        FindAdapter<Product>,
        SearchAdapter<Product>,
        CountAdapter<Product> {

    @Override
    default CriteriaTranslator<Product, ProductCriteria> getCriteriaTranslator() {
        return new ProductCriteriaTranslator();
    }
}
