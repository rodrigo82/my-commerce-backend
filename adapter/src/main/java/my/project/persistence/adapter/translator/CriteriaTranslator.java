package my.project.persistence.adapter.translator;

import my.project.port.Criteria;
import org.springframework.data.jpa.domain.Specification;

public interface CriteriaTranslator<T, C extends Criteria<T>> {

    Specification<T> translate(C criteria);

}
