package my.project.persistence.adapter;

import my.project.adapter.PagedCriteria;
import my.project.persistence.adapter.translator.CriteriaTranslator;
import my.project.port.Criteria;
import my.project.port.Search;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface SearchAdapter<T> extends Search<T> {

    <C extends Criteria<T>> CriteriaTranslator<T, C> getCriteriaTranslator();

    Page<T> findAll(Specification<T> var1, Pageable var2);

    List<T> findAll(Specification<T> var1);

    @Override
    default <C extends Criteria<T>> Iterable<T> search(C criteria) {
        if (criteria instanceof PagedCriteria) {
            var pagedCriteria = (PagedCriteria<T>) criteria;
            return findAll(getCriteriaTranslator().translate(pagedCriteria.getCriteria()), pagedCriteria.getPageable());
        } else {
            return findAll(getCriteriaTranslator().translate(criteria));
        }
    }
}
