package my.project.persistence.adapter.translator;

import my.project.domain.model.Section;
import my.project.domain.criteria.SectionCriteria;
import org.springframework.data.jpa.domain.Specification;

public class SectionCriteriaTranslator implements CriteriaTranslator<Section, SectionCriteria> {
    @Override
    public Specification<Section> translate(SectionCriteria criteria) {
        return all();
    }

    private Specification<Section> all() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id"));
    }
}
