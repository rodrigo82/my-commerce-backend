package my.project.persistence.adapter;

import my.project.persistence.adapter.translator.CriteriaTranslator;
import my.project.port.Count;
import my.project.port.Criteria;
import org.springframework.data.jpa.domain.Specification;

public interface CountAdapter<T> extends Count<T> {

    <C extends Criteria<T>> CriteriaTranslator<T, C> getCriteriaTranslator();

    long count(Specification<T> var1);

    @Override
    default <C extends Criteria<T>> Long count(C criteria) {
        return count(getCriteriaTranslator().translate(criteria));

    }

}
