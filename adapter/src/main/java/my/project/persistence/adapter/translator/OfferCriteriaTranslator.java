package my.project.persistence.adapter.translator;

import my.project.domain.model.Offer;
import my.project.domain.criteria.OfferCriteria;
import org.springframework.data.jpa.domain.Specification;

public class OfferCriteriaTranslator implements CriteriaTranslator<Offer, OfferCriteria> {
    @Override
    public Specification<Offer> translate(OfferCriteria criteria) {
        return all();
    }

    private Specification<Offer> all() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id"));
    }
}
