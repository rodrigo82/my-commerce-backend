package my.project.persistence.adapter;

import my.project.port.Write;

public interface WriteAdapter<T> extends Write<T> {

    T save(T value);

    @Override
    default T write(T value) {
        return save(value);
    }
}
