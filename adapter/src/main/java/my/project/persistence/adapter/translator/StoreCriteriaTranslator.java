package my.project.persistence.adapter.translator;

import my.project.domain.model.Store;
import my.project.domain.criteria.StoreCriteria;
import org.springframework.data.jpa.domain.Specification;

public class StoreCriteriaTranslator implements CriteriaTranslator<Store, StoreCriteria> {
    @Override
    public Specification<Store> translate(StoreCriteria criteria) {
        return all();
    }

    private Specification<Store> all() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id"));
    }
}
