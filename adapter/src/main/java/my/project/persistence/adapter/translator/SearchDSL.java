package my.project.persistence.adapter.translator;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.ObjectUtils;

import java.util.function.Function;

import static org.springframework.util.ObjectUtils.isEmpty;


public class SearchDSL {

    public static <T, C> Specification<T> with(C value, Function<C, Specification<T>> criteria, Specification<T> specification) {
        if (!isEmpty(value)) {
            specification = specification.and(criteria.apply(value));
        }
        return specification;
    }

    public static <T> Specification<T> when(boolean value, Specification<T> criteria, Specification<T> specification) {
        return value ? specification.and(criteria) : specification;
    }

    public static String contains(String q) {
        return ("%" + q + "%").toLowerCase();
    }

}
