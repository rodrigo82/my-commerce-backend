package my.project.persistence.adapter.translator;

import my.project.domain.criteria.ProductCriteria;
import my.project.domain.model.Product;
import org.springframework.data.jpa.domain.Specification;

import static my.project.persistence.adapter.translator.SearchDSL.contains;
import static my.project.persistence.adapter.translator.SearchDSL.with;

public class ProductCriteriaTranslator implements CriteriaTranslator<Product, ProductCriteria> {
    @Override
    public Specification<Product> translate(ProductCriteria criteria) {
        var specification = all();

        specification = with(criteria.getNamePart(), this::namePart, specification);

        specification = with(criteria.getSection(), this::section, specification);


        return specification;
    }

    private Specification<Product> section(String section) {
        return (root, criteriaQuery, criteriaBuilder) -> {
            var sectionJoin = root.join("section");
            return criteriaBuilder.equal(sectionJoin.get("id"), section);
        };
    }

    private Specification<Product> namePart(String namePart) {
        return (root, criteriaQuery, cb) -> cb.like(cb.lower(root.get("name")), contains(namePart));
    }

    private Specification<Product> all() {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.isNotNull(root.get("id"));
    }
}
