package my.project.persistence.adapter;

import my.project.port.Find;

import java.util.Optional;

public interface FindAdapter<T> extends Find<T> {

    Optional<T> findById(String id);

    @Override
    default Optional<T> find(String id) {
        return findById(id);
    }
}
