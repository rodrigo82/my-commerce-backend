package my.project.domain.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ItemSetTest {

    private final ItemSet itemSet = mock(ItemSet.class);

    @BeforeEach
    public void init() {
        when(itemSet.getTotalValue()).thenCallRealMethod();
    }

    @Test
    void itemSetIsEmpty() {
        when(itemSet.getItems()).thenReturn(emptySet());
        assertThat(itemSet.getTotalValue()).isEqualTo(ZERO);
    }

    @Test
    void itemSetIsNotEmpty1() {
        when(itemSet.getItems()).thenReturn(Set.of(
                new Item().withPrice(valueOf(20.00)).withQuantity(1)
        ));
        assertThat(itemSet.getTotalValue()).isEqualTo(valueOf(20.00));
    }

    @Test
    void itemSetIsNotEmpty2() {
        when(itemSet.getItems()).thenReturn(Set.of(
                new Item().withPrice(valueOf(20.00)).withQuantity(1),
                new Item().withPrice(valueOf(10.00)).withQuantity(4)
        ));
        assertThat(itemSet.getTotalValue()).isEqualTo(valueOf(60.00));
    }
}
