package my.project.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

import static java.time.LocalDateTime.now;
import static my.project.domain.model.Payment.Status.WAITING;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class Payment {

    public enum Status {
        WAITING, CONFIRMED, DENIED
    }

    @NotNull
    private Status status = WAITING;

    @NotNull
    private LocalDateTime date = now();

}
