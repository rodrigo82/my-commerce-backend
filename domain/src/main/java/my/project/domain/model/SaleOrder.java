package my.project.domain.model;

import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static java.time.LocalDateTime.now;
import static lombok.AccessLevel.NONE;
import static lombok.AccessLevel.PROTECTED;
import static my.project.domain.model.IdSupplier.get;
import static my.project.domain.model.SaleOrder.Status.WAITING_PAYMENT;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class SaleOrder implements ItemSet{

    public enum Status {
        WAITING_PAYMENT, CONFIRMED, DELIVERING, FINISHED
    }

    private String id = get();

    @NotNull
    private Customer customer;

    @NotNull
    private LocalDateTime date = now();

    @NotNull
    private Status status = WAITING_PAYMENT;

    @NotEmpty
    private Set<Item> items = new HashSet<>();

    @NotNull
    private Payment payment = new Payment();

}
