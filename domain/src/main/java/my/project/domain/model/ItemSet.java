package my.project.domain.model;

import java.math.BigDecimal;
import java.util.Set;

import static java.math.BigDecimal.ZERO;

public interface ItemSet {

    Set<Item> getItems();

    default BigDecimal getTotalValue() {
        return getItems().stream()
                .map(Item::getTotalValue)
                .reduce(BigDecimal::add).orElse(ZERO);
    }

}
