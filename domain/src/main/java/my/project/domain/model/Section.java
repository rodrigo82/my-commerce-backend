package my.project.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotBlank;

import static my.project.domain.model.IdSupplier.get;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class Section {

    private String id = get();

    @NotBlank
    private String name = "";

}
