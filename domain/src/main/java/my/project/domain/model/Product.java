package my.project.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import static my.project.domain.model.IdSupplier.get;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class Product {

    private String id = get();

    @NotBlank
    private String name = "";

    @NotBlank
    private String description = "";

    @NotNull
    private Section section;

}
