package my.project.domain.model;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import static lombok.AccessLevel.NONE;
import static lombok.AccessLevel.PROTECTED;
import static my.project.domain.model.IdSupplier.get;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class Cart implements ItemSet {

    private String id = get();

    @NotNull
    private Set<Item> items = new HashSet<>();

}
