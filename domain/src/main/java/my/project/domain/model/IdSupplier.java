package my.project.domain.model;

import static java.util.UUID.randomUUID;

public class IdSupplier {
    public static String get() {
        return randomUUID().toString();
    }
}
