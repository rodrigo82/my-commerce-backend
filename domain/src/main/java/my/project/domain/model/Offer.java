package my.project.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static my.project.domain.model.IdSupplier.get;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class Offer {

    private String id = get();

    @NotNull
    private Product product;

    @NotNull
    private Store store;

    @NotNull
    @Positive
    private BigDecimal price = ZERO;

}
