package my.project.domain.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

import static java.math.BigDecimal.ZERO;
import static java.math.BigDecimal.valueOf;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class Item {

    @NotNull
    @Positive
    private BigDecimal price = ZERO;

    @NotNull
    private Offer offer;

    @NotNull
    @Positive
    private Integer quantity = 1;

    public BigDecimal getTotalValue() {
        return valueOf(quantity).multiply(price);
    }
}
