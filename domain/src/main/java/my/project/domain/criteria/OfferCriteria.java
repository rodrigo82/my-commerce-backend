package my.project.domain.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import my.project.domain.model.Offer;
import my.project.port.Criteria;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class OfferCriteria implements Criteria<Offer> {

    private String product;
    private String store;

}
