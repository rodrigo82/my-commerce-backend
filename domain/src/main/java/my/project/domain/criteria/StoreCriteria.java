package my.project.domain.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import my.project.domain.model.Store;
import my.project.port.Criteria;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class StoreCriteria implements Criteria<Store> {

    private String namePart;

}
