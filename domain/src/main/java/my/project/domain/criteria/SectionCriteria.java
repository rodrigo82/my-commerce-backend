package my.project.domain.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import my.project.domain.model.Section;
import my.project.port.Criteria;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class SectionCriteria implements Criteria<Section> {

    private String namePart;

}
