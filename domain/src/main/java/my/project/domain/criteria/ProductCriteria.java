package my.project.domain.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import my.project.domain.model.Product;
import my.project.port.Criteria;

@AllArgsConstructor
@NoArgsConstructor
@Data
@With
public class ProductCriteria implements Criteria<Product> {

    private String namePart;

    private String section;

}
