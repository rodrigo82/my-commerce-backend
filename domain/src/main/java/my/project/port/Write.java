package my.project.port;

@FunctionalInterface
public interface Write<T> {
    T write(T value);
}
