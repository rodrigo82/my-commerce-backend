package my.project.port;

@FunctionalInterface
public interface Count<T> {
    <C extends Criteria<T>> Long count(C criteria);
}
