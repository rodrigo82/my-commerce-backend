package my.project.port;

@FunctionalInterface
public interface Search<T> {
    <C extends Criteria<T>> Iterable<T> search(C criteria);
}
