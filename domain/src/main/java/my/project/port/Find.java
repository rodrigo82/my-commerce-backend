package my.project.port;

import java.util.Optional;

@FunctionalInterface
public interface Find<T> {
    Optional<T> find(String id);
}
